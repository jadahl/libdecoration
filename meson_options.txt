option('demo', type: 'boolean', value: true, description: 'build cairo and EGL client examples')

option('dbus', type: 'feature', value: 'enabled', description: 'use D-Bus to fetch cursor settings')
